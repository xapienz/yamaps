package ru.xapienz.maps;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.AsyncTask;

import java.io.*;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedList;

/**
 * User: xapienz
 * Date: 04.02.13
 * Time: 0:44
 */
public class Cache {
    private final int BUFFER_SIZE = 4096;
    private final int MEMORY_CACHE_SIZE;

    private Context context;
    private HashMap<Point, Bitmap> cache = new HashMap<Point, Bitmap>();
    private LinkedList<Point> cacheQueue = new LinkedList<Point>();     // needed to determine old items in cache
    private DownloadFinishedListener downloadFinishedListener;          // ask MapView to redraw tile

    Downloader downloader;

    public Cache(Context context, int cacheSize) {
        MEMORY_CACHE_SIZE = cacheSize;
        this.context = context;
    }

    public void setDownloadFinishedListener(DownloadFinishedListener dfl) {
        downloadFinishedListener = dfl;
    }

    /**
     * @param x x coordinate
     * @param y y coordinate
     * @return Bitmap with tile from memory cache, or null if no tile in cache
     */
    public Bitmap get(int x, int y) {
        return cache.get(new Point(x,y));
    }

    /**
     * Starts downloading of necessary tiles in rectangle set by x, y, w, h
     * @param x left coordinate of rectangle
     * @param y top coordinate of rectangle
     * @param w width of rectangle
     * @param h height of rectangle
     */
    public void prepare(int x, int y, int w, int h) {
        if(downloader != null && downloader.getStatus()==AsyncTask.Status.RUNNING)
            downloader.cancel(false);
        downloader = new Downloader();
        downloader.execute(x, y, w, h);
    }

    /**
     * Downloader class is async task to download (if necessary) a pack of tiles and save them into cache
     */
    public class Downloader extends AsyncTask<Integer, Point, Void> {
        public final static String API_PATH = "http://vec.maps.yandex.net/tiles?l=map&v=2.21.0&x=%d&y=%d&z=10";
        public final static String FILE = "tile%d_%d.png";

        @Override
        protected Void doInBackground(Integer... rect) {
            if(rect.length!=4) return null;
            // going for points by spiral from center of rectangle for better UX
            for(Point point: new SpiralRectangle(rect[0], rect[1], rect[2], rect[3])) {
                    synchronized (cache) {
                        if(cache.get(point)==null) {
                            // not in memory? - try from file or internet
                            Bitmap bitmap = getFromExternal(point.x, point.y);
                            if(bitmap!=null) {
                                cache.put(point, bitmap);
                                cacheQueue.add(point);
                                while(cache.size()> MEMORY_CACHE_SIZE) {
                                    // cleaning cache
                                    Point pointRemoved = cacheQueue.poll();
                                    // if point is in current rectangle, just move it to the top of queue:
                                    if(pointRemoved.x>=rect[0] && pointRemoved.x<rect[0]+rect[2]
                                            && pointRemoved.y>rect[1] && pointRemoved.y<rect[1]+rect[3])
                                        cacheQueue.add(pointRemoved);
                                    else {
                                        Bitmap bitmapRemoved = cache.remove(pointRemoved);
                                        // allow MapView draw Bitmap (if needed) even if it is gonna be recycled:
                                        synchronized (bitmapRemoved) {
                                            bitmapRemoved.recycle();
                                        }
                                    }
                                }
                                publishProgress(point);
                            }
                        }
                        else {
                            // move existing object to top of queue
                            cacheQueue.remove(point);
                            cacheQueue.add(point);
                        }
                    }
                    if(isCancelled()) break;    // stop thread
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Point... points) {
            super.onProgressUpdate(points);
            if(points.length==1 && downloadFinishedListener!=null)
                downloadFinishedListener.downloadFinished(points[0].x, points[0].y);
        }

        /**
         * Getting Bitmap from either file or url
         * @param x x coordinate of tile
         * @param y y coordinate of tile
         * @return necessary bitmap
         */
        private Bitmap getFromExternal(int x, int y) {
            Bitmap bitmap = getFromFile(x, y);
            if(bitmap==null) {              // no in file?
                saveFromUrl(x, y);          // download it to file...
                bitmap = getFromFile(x, y); // and get from there
            }
            return bitmap;
        }

        /**
         * Getting Bitmap from file
         * @param x x coordinate of tile
         * @param y y coordinate of tile
         * @return necessary bitmap
         */
        private Bitmap getFromFile(int x, int y) {
            String file = String.format(FILE, x, y);
            FileInputStream fis = null;
            try {
                fis = new FileInputStream(new File(context.getCacheDir(), file));
                return BitmapFactory.decodeStream(fis);
            }
            catch(IOException ex) {
                return null;
            }
            finally {
                if(fis!=null) try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        /**
         * Download tile from the Internet and save it into file cache
         * @param x x coordinate of tile
         * @param y y coordinate of tile
         * @return saved or not
         */
        private boolean saveFromUrl(int x, int y) {
            String from = String.format(API_PATH, x, y);
            String to = String.format(FILE, x, y);
            InputStream is = null;
            FileOutputStream fos = null;
            byte buffer[] = new byte[BUFFER_SIZE];
            try {
                URL url = new URL(from);
                is = url.openStream();
                fos = new FileOutputStream(new File(context.getCacheDir(), to));
                int size;
                while((size = is.read(buffer)) != -1) {
                    fos.write(buffer, 0, size);
                }
            }
            catch(IOException e) {
                return false;
            }
            finally {
                if(is!=null) try {
                    is.close();
                } catch (IOException e) {
                    return false;
                }
                if(fos!=null) try {
                    fos.close();
                } catch (IOException e) {
                    return false;
                }
            }
            return true;
        }
    }

    public static interface DownloadFinishedListener {
        public void downloadFinished(int x, int y);
    }
}
