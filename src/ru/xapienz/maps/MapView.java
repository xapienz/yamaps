package ru.xapienz.maps;

import android.content.Context;
import android.graphics.*;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/**
 * User: xapienz
 * Date: 31.01.13
 * Time: 22:19
 */
public class MapView extends View {
    private final int TILE_SIZE = 256;
    private final int MARGIN = 2;
    private final int MIN_X = 600*TILE_SIZE;
    private final int MAX_X = 700*TILE_SIZE;
    private final int MIN_Y = 300*TILE_SIZE;
    private final int MAX_Y = 400*TILE_SIZE;

    private float x = 617*256, y=319*256;
    private int lastHash = 0;
    private Cache cache;

    public MapView(Context context) {
        super(context);
    }

    public MapView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MapView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * Set initial values: count of tiles, cache, cache listener
     */
    private void init() {
        cache = new Cache(getContext(), 3*(getHeight()/TILE_SIZE+2*MARGIN)*(getWidth()/TILE_SIZE+2*MARGIN));
        cache.setDownloadFinishedListener(new Cache.DownloadFinishedListener() {
            @Override
            public void downloadFinished(int x, int y) {
                invalidate();
            }
        });
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if(cache==null) init();

        // top-left visible tile
        int tileX = (int)(x/TILE_SIZE);
        int tileY = (int)(y/TILE_SIZE);

        // coordinates of top-left visible tile
        float dx = tileX*TILE_SIZE - x;
        float dy = tileY*TILE_SIZE - y;

        // count of visible tiles
        int tileXCount = (int)Math.ceil((getWidth()-dx)/TILE_SIZE);
        int tileYCount = (int)Math.ceil((getHeight()-dy)/TILE_SIZE);

        // check if need to reload some tiles
        int hash = hash(tileX, tileY, tileXCount, tileYCount);
        if(hash!=lastHash) {
            cache.prepare(tileX-MARGIN, tileY-MARGIN, tileXCount+2*MARGIN, tileYCount+2*MARGIN);
            lastHash = hash;
        }

        // draw tiles
        for(int j=0; j<tileXCount; j++) {
            for(int i=0; i<tileYCount; i++) {
                Bitmap bitmap = cache.get(tileX+j,tileY+i);
                if(bitmap!=null) synchronized (bitmap) {
                    // as bitmap can be recycled somewhere here in another thread, locking it and checking
                    if(!bitmap.isRecycled())
                        canvas.drawBitmap(bitmap, dx+j*TILE_SIZE, dy+i*TILE_SIZE, null);
                }
            }
        }
    }

    /**
     * simple 4-value hash
     */
    private int hash(int a, int b, int c, int d) {
        return (((((a*500)+b)*500)+c)*500+d);
    }

    float ox, oy;
    boolean pressed = false;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        super.onTouchEvent(event);
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                ox = event.getX();
                oy = event.getY();
                pressed = true;
                break;
            case MotionEvent.ACTION_UP:
                pressed = false;
                break;
            case MotionEvent.ACTION_MOVE:
                if(pressed) {
                    x+=(ox-event.getX());
                    y+=(oy-event.getY());
                    ox = event.getX();
                    oy = event.getY();

                    // check bounds
                    if(x<MIN_X) x = MIN_X;
                    if(x+getWidth()>MAX_X) x = MAX_X-getWidth();
                    if(y<MIN_Y) y = MIN_Y;
                    if(y+getHeight()>MAX_Y) y = MAX_Y-getHeight();

                    // repaint
                    invalidate();
                }
                break;
        }
        return true;
    }

}
