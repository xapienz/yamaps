package ru.xapienz.maps;

import android.graphics.Point;
import java.util.Iterator;

/**
 * User: xapienz
 * Date: 04.02.13
 * Time: 19:50
 */

public class SpiralRectangle implements Iterable<Point> {
    private int x, y, w, h;

    public SpiralRectangle(int x, int y, int w, int h) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }

    /**
     * artful iterator for going over rectangle by spiral from center to outer
     * @return
     */
    @Override
    public Iterator<Point> iterator() {
        return new Iterator<Point>() {
            private int cx, cy;
            private int maxStep = 0;
            private int step = 0;
            private int gone = 0;
            private int direction = 0;  // 0 is right, 1 is down, 2 is left, 3 is up

            @Override
            public boolean hasNext() {
                if(gone==w*h) return false;
                return true;
            }

            @Override
            public Point next() {
                if(maxStep==0) {
                    cx = x+(w-1)/2;
                    cy = y+(h-1)/2;
                    maxStep = 1;
                    step = 0;
                }
                else {
                    switch (direction) {
                        case 0:
                            if(cx+1>=x+w) {
                                rotate();
                                cy += maxStep;
                                step = maxStep;
                            }
                            else {
                                cx++;
                                step++;
                            }
                            break;
                        case 1:
                            if(cy+1>=y+h) {
                                rotate();
                                cx -= maxStep;
                                step = maxStep;
                            }
                            else {
                                cy++;
                                step++;
                            }
                            break;
                        case 2:
                            if(cx<=x) {
                                rotate();
                                cy -= maxStep;
                                step = maxStep;
                            }
                            else {
                                cx--;
                                step++;
                            }
                            break;
                        case 3:
                            if(cy<=y) {
                                rotate();
                                cx -= maxStep;
                                step = maxStep;
                            }
                            else {
                                cy--;
                                step++;
                            }
                            break;
                    }
                    if(step == maxStep) rotate();
                }
                gone++;
                return new Point(cx, cy);
            }


            private void rotate() {
                direction = (direction+1)%4;
                step = 0;
                if(direction==0 || direction==2) maxStep++;
            }

            @Override
            public void remove() {}
        };
    }
}
